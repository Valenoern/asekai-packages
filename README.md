Packages for the [asekai](https://codeberg.org/Valenoern/zensekai-demo)/zensekai project.


## what am I looking at?

This is a binary package (compiled program) repository built on the experimental [gitrelease](https://codeberg.org/Valenoern/gitrelease) system.  
You shouldn't clone it without arguments or you'll get every single version for every operating system.

You can safely get just the latest packages by using the `https` git clone url with a "shallow" option,  
ex.: `git clone --depth 1 https://codeberg.org/Valenoern/asekai-packages.git`

(though, the files in this repository are big enough you may instead want to look for a way to just download a single package file from gitea. clicking on the file and "**View Raw**" should do the trick)


## specific instructions

The godot-06-30 release was put up for debugging the process to export godot for the pinephone.
(As of now that still isn't done and godot does _not_ run on Ubuntu Touch.)  
To run the asekai demo it's recommended you download the 2020-04-04 release, which this repo has already been returned to and the commands above should give you.

on either windows or debian, you should be able to run the `zensekai` file in the `zensekai` folder, or the `bat`/`sh` at the top of the archive if there is one. See the [zensekai docs page](https://valenoern.codeberg.page/zensekai/docs/docs.html) for more information.

(unfortunately, both this demo and its docs are about a year out of date now and it will be a while before I get back to working on them again, so I can't test anything at this time.)


## maintainer contact

`valenoern ＠ distributary.network` - public email \[might be down currently\]  
[valenoern@floss.social](https://floss.social/@Valenoern) - mastodon microblog  
[Valenoern@twitter.com](https://twitter.com/valenoern) - not checked often. use only if the others are unworkable
